from flask import Flask
from flask_cors import CORS
from waitress import serve
from config import load_config
from context import AppContext

from routes import (
    register_root_route,
    register_exception_handlers,
    register_example_routes,
    register_health_routes
)


def create_app():
    # Set up app
    app = Flask(__name__)
    config = load_config(app)
    context = AppContext(config=config)

    # Register routes
    register_exception_handlers(app=app)
    register_root_route(app=app, context=context)
    register_example_routes(app=app, context=context)
    register_health_routes(app=app, context=context)

    CORS(app)
    return app


if __name__ == '__main__':
    _app = create_app()
    environment = _app.config.get("ENVIRONMENT")
    host = _app.config.get("HOST")
    port = _app.config.get("PORT")

    if environment == "prod":
        print("Starting app in production mode")
        serve(app=_app, host=host, port=port, threads=4)
    else:
        print("Starting app in development mode")
        _app.run(host=host, port=port, threaded=True)

