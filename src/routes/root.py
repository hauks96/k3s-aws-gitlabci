from flask import Blueprint, Flask
from context import AppContext


def register_root_route(app: Flask, context: AppContext):
    # Create a blueprint that can be added to app manually
    root_bp = Blueprint(
        name="root",
        import_name=__name__,
        url_prefix=context.get_path_prefix()
    )

    # Define a route for the root endpoint
    @root_bp.route("", methods=["GET"])
    def index():
        return {
            "message": "Ok",
            "routes": [
                {"method": "GET", "route": "/health"},
                {"method": "GET", "route": "/example"}
            ]
        }, 200

    # Register the route on the app
    app.register_blueprint(root_bp)

