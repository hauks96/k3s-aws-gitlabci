from flask import Blueprint, Flask, request
from context import AppContext


def register_example_routes(app: Flask, context: AppContext):
    # Create a blueprint that can be added to app manually
    example_bp = Blueprint(
        name="example",
        import_name=__name__,
        url_prefix=context.get_path_prefix()
    )

    # Define a route on this blueprint
    @example_bp.route("/example", methods=["GET"])
    def index():
        return {"message": context.get_message()}, 200

    @example_bp.route("/example/<int:parameter>", methods=["POST"])
    def post_example(parameter: int):
        data = request.json
        example = data.get("example") if type(data) == dict else None

        if example is None:
            return {
                "message": "Key 'example' missing in request body"
            }, 400

        return {
            "message": f"You sent '{example}' to /example/{parameter}"
        }, 200

    # Register the route on the app
    app.register_blueprint(example_bp)

