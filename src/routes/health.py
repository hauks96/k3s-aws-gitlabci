import json

from flask import Blueprint, Flask, Response
from context import AppContext
from requests import request as py_request
import requests as set_request

# Hack to prevent slow IPV6 requests
set_request.packages.urllib3.util.connection.HAS_IPV6 = False


class HealthChecker:
    @classmethod
    def _get_health_state(cls, healthy: bool, status_code: int, message: str):
        return {
            "healthy": healthy,
            "status_code": status_code,
            "message": message,
        }

    @classmethod
    def is_healthy(cls, health_state: dict) -> bool:
        return health_state["healthy"]

    @classmethod
    def check_endpoint(cls, endpoint_url: str, method: str, body: any) -> dict:
        try:
            response = py_request(
                method=method,
                url=endpoint_url,
                data=json.dumps(body),
                headers={"content-type": "application/json; charset=utf8"}
            )
        except Exception as error:
            return cls._get_health_state(
                healthy=False,
                status_code=500,
                message=f"Unable to make request to {endpoint_url}. Failed with {str(error)}"
            )
        if response.status_code != 204:
            try:
                response_data = response.json()
            except json.JSONDecodeError as error:
                return cls._get_health_state(
                    healthy=False,
                    status_code=500,
                    message=f"Unable to parse response to json. Failed with {str(error)}"
                )
        else:
            response_data = {}

        return cls._get_health_state(
            healthy=(200 <= response.status_code < 400),
            status_code=response.status_code,
            message=response_data.get("message", "none")
        )


def register_health_routes(app: Flask, context: AppContext):
    # Create a blueprint that can be added to app manually
    health_bp = Blueprint(
        name="health",
        import_name=__name__,
        url_prefix=context.get_path_prefix()
    )

    # Define the base path on this blueprint
    @health_bp.route("/health", methods=["GET"])
    def index():
        return {
            "message": "Ok",
            "routes": [
                {"method": "GET", "route": "health/liveness-probe"},
                {"method": "GET", "route": "health/readiness-probe"}
            ]
        }, 200

    @health_bp.route("/health/readiness-probe", methods=["GET"])
    def readiness():
        return Response(status=204)

    @health_bp.route("/health/liveness-probe", methods=["GET"])
    def liveness():
        endpoints = [
            {"method": "GET", "route": "", "body": None},
            {"method": "GET", "route": "/example", "body": None},
            {"method": "POST", "route": "/example/1", "body": {"example": "Hello there!"}},
            {"method": "GET", "route": "/health", "body": None},
            {"method": "GET", "route": "/health/readiness-probe", "body": None},
        ]

        healthy = 1
        unhealthy = 0
        endpoint_health_states = {}
        for endpoint in endpoints:
            route = endpoint["route"]
            route_key = "/" if route == "" else route

            endpoint_health_states[route_key] = HealthChecker.check_endpoint(
                endpoint_url=context.get_url(endpoint["route"]),
                method=endpoint["method"],
                body=endpoint["body"]
            )
            if HealthChecker.is_healthy(endpoint_health_states[route_key]):
                healthy += 1
            else:
                unhealthy += 1

        health_percentage = round((healthy / (healthy + unhealthy)) * 100, 2)
        return {
                   "message": "Ok",
                   "state": endpoint_health_states,
                   "healthy": f"{health_percentage}%"
               }, 500 if health_percentage != 100 else 200

    # Register the route on the app
    app.register_blueprint(health_bp)
