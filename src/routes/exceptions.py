from flask import Blueprint, Flask
from werkzeug.exceptions import BadRequest


def register_exception_handlers(app: Flask):

    exception_handlers_bp = Blueprint(
        name="exceptions",
        import_name=__name__
    )

    # Add your custom exception handlers here

    @exception_handlers_bp.app_errorhandler(404)
    def invalid_route(e):
        info_message = "Route does not exist."
        app.logger.info(msg=info_message)
        return {
            "message": "Route does not exist."
        }, 404

    @exception_handlers_bp.app_errorhandler(BadRequest)
    def not_json_error(error: BadRequest):
        error_message = "Flask: request.json could not be parsed to json."
        app.logger.error(msg=error_message, exc_info=error)
        return {
            "message": error_message
        }, 400

    @exception_handlers_bp.app_errorhandler(Exception)
    def unexpected_exception_handler(error: Exception):
        exception_message = "An unknown exception occurred during the handling of the request."
        app.logger.exception(msg=exception_message, exc_info=error)
        return {
            "message": exception_message
        }, 500

    app.register_blueprint(exception_handlers_bp)
