import os

from flask import Flask


class _BaseConfig:
    HOST = "0.0.0.0"
    PORT = 3000
    API_VERSION = "0.0.1"
    DEBUG_ENABLED = os.environ["FLASK_DEBUG"] == "True"
    ENVIRONMENT = "none"

    def __init__(self):
        print(
          f"Environment: {self.ENVIRONMENT}\n"
          f"Host: {self.HOST}\n"
          f"Port: {self.PORT}\n"
          f"Debug enabled: {self.DEBUG_ENABLED}\n"
          f"API version: {self.API_VERSION}"
        )


class DevConfig(_BaseConfig):
    ENVIRONMENT = "dev"

    def __init__(self):
        super().__init__()


class ProdConfig(_BaseConfig):
    ENVIRONMENT = "prod"

    def __init__(self):
        super().__init__()
        self.API_VERSION = os.environ["API_VERSION"]


class TestConfig(_BaseConfig):
    ENVIRONMENT = "test"
    TESTING = "true"

    def __init__(self):
        super().__init__()
        print(f"Testing enabled: {self.TESTING}")


config_map = {
    "dev": DevConfig,
    "prod": ProdConfig,
    "test": TestConfig
}


def load_config(app: Flask):
    environment = os.environ["ENVIRONMENT"]
    config = config_map.get(environment, ProdConfig)()
    app.config.from_object(config)

    return config
