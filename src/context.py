from config import _BaseConfig


class AppContext:
    def __init__(self, config: _BaseConfig):
        self.__config = config
        self.__message = "This is a message from the context!"

    def get_path_prefix(self):
        return f"/api/v{self.__config.API_VERSION}"

    def get_url(self, endpoint_route):
        host = self.__config.HOST
        if host == "0.0.0.0" or host == "127.0.0.1":
            host = "localhost"

        return f"http://{host}:{self.__config.PORT}{self.get_path_prefix()}{endpoint_route}"

    def get_message(self):
        return self.__message
