# Cluster infrastructure setup
Note that these instructions are highly AWS specific as I do not have experience with anything else. However, they 
should be translatable to any infrastructure management tool with some googling skills.

## What you need
The amount of virtual machines you need for various cluster setups. (One node = One VM/EC2)

### Highly available setup (Recommended)
Master nodes are only used for managing the cluster state

| Node type (VM)     | Amount | Purpose                                                                |
|--------------------|--------|------------------------------------------------------------------------|
| Manager Node       | 1      | Bastion & To interact with the cluster manually through kubectl        |
| Master Nodes       | 3+     | To manage cluster resources                                            | 
| Worker Nodes       | 2+     | To run applications and services in clusters                           |
| Gitlab runner Node | 1      | To run kubectl commands from gitlab ci on a cluster in private network |

### Without high availability (Single point of failure)
One master node and at least one worker node

| Node type (VM)     | Amount | Purpose                                                                |
|--------------------|--------|------------------------------------------------------------------------|
| Manager Node       | 1      | Bastion & To interact with the cluster manually through kubectl        |
| Master Nodes       | 1      | To manage cluster resources                                            | 
| Worker Nodes       | 1+     | To run applications and services in clusters                           |
| Gitlab runner Node | 1      | To run kubectl commands from gitlab ci on a cluster in private network |

### High availability (Less VM's)
Schedule workloads on master nodes

| Node type (VM)      | Amount | Purpose                                                                |
|---------------------|--------|------------------------------------------------------------------------|
| Manager Node        | 1      | Bastion & To interact with the cluster manually through kubectl        |
| Master/Worker Nodes | 3+     | To manage cluster resources and to run applications and services       |
| Gitlab runner Node  | 1      | To run kubectl commands from gitlab ci on a cluster in private network |

### The smallest setup possible
The broke man's setup

| Node type (VM)                   | Amount | Purpose                                                         |
|----------------------------------|--------|-----------------------------------------------------------------|
| Manager Node                     | 1      | Bastion & To interact with the cluster manually through kubectl |
| Master/Worker/Gitlab Runner Node | 1      | Run entire cluster and gitlab runner on single node             |

## Before you start
Select one of the possible setups in the tables above and create the virtual machines according to the specifications below. 
Note that these are the **minimum** requirements. You should especially consider increasing the storage capacity of the 
mounts if you intend to run more than a handful of services. If you are combining multiple roles into a single node, 
such as if you were to run workloads on a master node, you can add the hardware specs of the node types together to 
determine what specs you need.

| Node type          | Subnet                                 | Operating system    | Specs      | Storage                |              
|--------------------|----------------------------------------|---------------------|------------|------------------------|
| Manager Node       | Public                                 | Ubuntu Server 22.04 | t2 micro+  | 8-16Gb Root            |
| Master Node        | Private (With internet access via NAT) | Ubuntu Server 22.04 | t2 micro+  | 8-16Gb Root            |
| Worker Node        | Private (With internet access via NAT) | Ubuntu Server 22.04 | t2 medium+ | 16Gb Root, 64Gb+ Mount |
| Gitlab Runner Node | Private (With internet access via NAT) | Ubuntu Server 22.04 | t2 small+  | 16Gb Root, 64Gb+ Mount |

Each virtual machine should:
- Have a descriptive name
- Have a key pair for SSH access
- Have SSH with a password disabled (Public key only)
- Be up-to-date (`sudo apt-get update -y && sudo apt-get upgrade`)

## 1. Networking
How the network setup should look like for your kubernetes cluster


### Enable internet access for private subnets (VPC in AWS)
For our cluster to stay as secure as possible, it is recommended to keep all nodes (except for the Manager node)
on private subnets. Our cluster however, will need internet access for things such as pulling images. We also want
our services to be able to use the internet if they need to. For this to work, we will have to set up NAT gateways 
on our public subnets that are reachable by our private subnets.

Here's a quick overview
- Set descriptive names on all subnets
- Set up elastic IP addresses
- Create one NAT gateway on each public subnet and associate elastic IP with it
- Create routing tables for the private subnets that point to the NAT gateway

#### Naming the subnets (Subnets in AWS)
We will be setting up a NAT gateway for each private subnet we plan to use, which is one in each availability zone
in AWS (1A, 1B, 1C). My account is in the eu-west region, but it may differ for each tenant. Here is how I have named 
my subnets. You can name them whatever you want, but I will be referencing these names in the coming steps.

| Subnet Names        | Availability Zone | Type    |
|---------------------|-------------------|---------|
| `PRIVATE-SUBNET-1A` | eu-west-1a        | Private |
| `PRIVATE-SUBNET-1B` | eu-west-1b        | Private |
| `PRIVATE-SUBNET-1C` | eu-west-1c        | Private |
| `PUBLIC-SUBNET-1A`  | eu-west-1a        | Public  |
| `PUBLIC-SUBNET-1B`  | eu-west-1b        | Public  |
| `PUBLIC-SUBNET-1C`  | eu-west-1c        | Public  |

#### Public IP addresses (Elastic IPs in AWS)
We need public IP addresses to associate with the NAT gateways. In AWS, we create Elastic IP addresses for each 
availability zone we plan to use.

| Elastic IP Name |
|-----------------|
| `IP-FOR-NAT-1A` |
| `IP-FOR-NAT-1B` |
| `IP-FOR-NAT-1C` |

#### NAT Gateways (NAT Gateway in AWS)
Now we create 3 NAT gateways and associate the public IP addresses (Elastic IPs in this case) we reserved earlier 
with them. These NAT gateways reside on the **public** subnets. We will set up routes for this in the routing table 
in a bit so that the private subnets can access the internet.

| NAT Name         | Type   | Associated IP address | Associated Subnet  |
|------------------|--------|-----------------------|--------------------|
| `NAT-GATEWAY-1A` | Public | `IP-FOR-NAT-1A`       | `PUBLIC-SUBNET-1A` |
| `NAT-GATEWAY-1B` | Public | `IP-FOR-NAT-1B`       | `PUBLIC-SUBNET-1B` |
| `NAT-GATEWAY-1C` | Public | `IP-FOR-NAT-1C`       | `PUBLIC-SUBNET-1C` |

#### Routing tables for private subnets (Route tables in AWS)
Now we create (or update) the routing table for each private subnet. Note that most likely there is already
a routing table associated with each of these private subnets. The default route in the table should have the
destination as the CIDR block of the VPC that your subnet is on and the target as `local`. 
(Destination: `VPC-CIDR`, Target: `local`)

| Route Table Name         | Associated Subnet   | Routes                                                                                              |
|--------------------------|---------------------|-----------------------------------------------------------------------------------------------------|
| `PRIVATE-ROUTE-TABLE-1A` | `PRIVATE-SUBNET-1A` | • Destination: `VPC-CIDR`, Target: `local` <br>• Destination: `0.0.0.0/0`, Target: `NAT-GATEWAY-1A` |
| `PRIVATE-ROUTE-TABLE-1B` | `PRIVATE-SUBNET-1B` | • Destination: `VPC-CIDR`, Target: `local` <br>• Destination: `0.0.0.0/0`, Target: `NAT-GATEWAY-1B` |
| `PRIVATE-ROUTE-TABLE-1C` | `PRIVATE-SUBNET-1C` | • Destination: `VPC-CIDR`, Target: `local` <br>• Destination: `0.0.0.0/0`, Target: `NAT-GATEWAY-1C` |

You should now be all set with internet connection on your private subnets! 
You can test it with curl or ping from an ec2 instance on a private subnet.

### Network security rules (Security Groups in AWS)
These are the security groups you will need for your cluster

| Security Group Name | Description                                                   | Inbound rules                                                                                                                                                                                  | Outbound rules                                  |
|---------------------|---------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------|
| `K3S-NODE`          | Allows cross communication between cluster nodes              | • Allow all traffic from `K3S-NODE`                                                                                                                                                            | • Allow all outbound traffic (to anywhere ipv4) |
| `K3S-WORKER`        | Allows inbound traffic from specific subnets and IP addresses | • Allow all traffic from CIDR blocks of subnets that should have access to the cluster<br>• Allow all traffic from office IP address<br>• If needed, allow some traffic from customers network | None                                            |
| `K3S-MANAGER`       | Allows SSH access from office network                         | • Allow all traffic from office IP                                                                                                                                                             | None                                            |

These are the rules that should be applied to each type of node (VM) you created

| Node type          | Security Groups          |
|--------------------|--------------------------|
| Master Node        | `K3S-NODE`               |
| Worker Node        | `K3S-NODE`,`K3S-WORKER`  |
| Manager Node       | `K3S-NODE`,`K3S-MANAGER` |
| Gitlab Runner Node | `K3S-NODE`               |

### Load Balancers (NLB in AWS)
If you have more than one worker node, you will want to set up load balancers to route traffic to **worker** nodes.
If you intend to only have one worker node, then you can route the traffic directly to the IP address of the worker node
and skip the load balancer setup entirely.

#### Target groups
The following target groups will be needed. All **worker nodes** should be added to **all** target groups.

| Target group name    | Target Type | Protocol | Port | Health Check     |
|----------------------|-------------|----------|------|------------------|
| `K3S-INTERNAL-HTTP`  | Instances   | TCP      | 80   | TCP, Port 80     |
| `K3S-INTERNAL-HTTPS` | Instances   | TCP      | 443  | TCP, **Port 80** |
| `K3S-PUBLIC-HTTP`    | Instances   | TCP      | 80   | TCP, Port 80     |
| `K3S-PUBLIC-HTTPS`   | Instances   | TCP      | 443  | TCP, **Port 80** |

#### Network Load Balancers
- The internal network load balancer will receive all traffic that is made to the **internal domains** of our cluster.
- The public network load balancer will receive all traffic that is made to the **public domains** of our cluster.

We will discuss domains in the next section but for now let's set up the NLB's.

| NLB name           | Scheme          | Subnets                                | Listeners                                                            |
|--------------------|-----------------|----------------------------------------|----------------------------------------------------------------------|
| `K3S-INTERNAL-NLB` | Internal        | Private (With internet access via NAT) | • TCP:80 -> `K3S-INTERNAL-HTTP`<br>• TCP:443 -> `K3S-INTERNAL-HTTPS` |
| `K3S-PUBLIC-NLB`   | Internet facing | Public                                 | • TCP:80 -> `K3S-PUBLIC-HTTP`<br>• TCP:443 -> `K3S-PUBLIC-HTTPS`     |


### Domain & DNS (Route53 in AWS)
Why do we need domains?
1. To be able to use ingresses to expose services
2. If you are using IPs and the node that you reference is not running, you're out of luck
3. Using IP addresses is a terrible inconvenience for all parties involved
4. You will find jesus

**Note**: cluster services can be exposed on both public and internal domains simultaneously

#### Internal domains (Private hosted zone in AWS)
These are the DNS records you will want to set up for services that are exposed **on the
internal network** of the tenant.

| Record name                      | Routes to                        | Type              | Hosted zone         | Example                                                                  |
|----------------------------------|----------------------------------|-------------------|---------------------|--------------------------------------------------------------------------|
| `<customer>.<internal-domain>`   | All **master node** IP addresses | A record          | `<internal-domain>` | • Hosted zone: `cluster.internal`<br>• Record: `demo.cluster.internal`   |
| `*.<customer>.<internal-domain>` | `K3S-INTERNAL-NLB`               | Wildcard A record | `<internal-domain>` | • Hosted zone: `cluster.internal`<br>• Record: `*.demo.cluster.internal` |

All internal services can now be deployed to the cluster with a domain `<service>.<customer>.<internal-domain>`
using ingress resource specifications. Services on the cluster are then reachable by every internal service on 
the tenant according to the inbound rules of `WORKER-NODE` via these domains.

**Note:** We will be referring to `<customer>.<internal-domain>` as `<YOUR_MASTER_NODE_DOMAIN>` in further setup steps.

#### Public domains (Public hosted zone in AWS)
We need a public domain for services that we want to expose on the internet. Note that although the service 
is exposed on the internet, access to the services is still limited by the inbound rules of the network security
groups.

| Record name                                  | Routes to                   | Type              | Hosted Zone                  | Example                                                                                 |
|----------------------------------------------|-----------------------------|-------------------|------------------------------|-----------------------------------------------------------------------------------------|
| `cluster.manager.<customer>.<public-domain>` | **Manager Node** IP address | A record          | `<customer>.<public-domain>` | • Hosted zone: `demo.epic.solutions`<br>• Record: `cluster.manager.demo.epic.solutions` |
| `*.<customer>.<public-domain>`               | `K3S-PUBLIC-NLB`            | Wildcard A record | `<customer>.<public-domain>` | • Hosted zone: `demo.epic.solutions`<br>• Record: `*.demo.epic.solutions`               |

- Record `cluster.manager.<customer>.<public-domain>`: Access limited by `MANAGER-NODE`
- Record `*.<customer>.<public-domain>`: Access limited by `WORKER-NODE`

Services can now be exposed to the internet with a domain `<service>.<customer>.<public-domain>` using ingress 
resource specifications.

If you need some help with setting up subdomain routing in AWS. For example if your public domain is `epic.solutions` 
and each tenant should have a subdomain hosted zone from it, take a look at [this link](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/dns-routing-traffic-for-subdomains.html).

In short, you have to create a new NS record on the root hosted zone (f.x `epic.solutions`) and add all the nameservers 
from the NS record of the hosted zone you created (f.x `demo.epic.solutions`) to it.

The cluster can deploy TLS certificates on its own through Let's Encrypt with the help of cert-manager. CertManager 
also takes care of automatically renewing any certificates before they expire. That is why we have set up TCP port 443 
forwarding on the public network load balancer and not TLS. The SSL is terminated at the cluster level through traefik, 
not at the external load balancer. We will go into that in the SSL section later.

And voilà! No more IP addresses nor certificate problems for applications EVER! (please)

## 2. Manager Node
The cluster management node serves two purposes
- Allows us to manually manage the cluster with kubectl
- Serves as a bastion host to our private network virtual machines

### Internal SSH config
To serve as a bastion for our cluster nodes, we need to set up a handy SSH config file.
Here is an example setup of an ssh config. The ssh key pair for each virtual machine was manually added into a 
directory `~/.ssh/keys/`. If the `.ssh` directory does not exist, simply create it.

The SSH config should be stored in a file at `~/.ssh/config` (config is the name of the file).

```bash
Host gitlab-runner
        HostName 123.123.123.1
        User ubuntu
        IdentityFile ~/.ssh/keys/gitlab-runner.pem

Host k3s-master-1
        HostName 123.123.123.2
        User ubuntu
        IdentityFile ~/.ssh/keys/MasterNode.pem

Host k3s-master-2
        HostName 123.123.123.3
        User ubuntu
        IdentityFile ~/.ssh/keys/MasterNode.pem

Host k3s-master-3
        HostName 123.123.123.4
        User ubuntu
        IdentityFile ~/.ssh/keys/MasterNode.pem

Host k3s-worker-1
        HostName 123.123.123.5
        User ubuntu
        IdentityFile ~/.ssh/keys/WorkerNode.pem

Host k3s-worker-2
        HostName 123.123.123.6
        User ubuntu
        IdentityFile ~/.ssh/keys/WorkerNode.pem

Host k3s-worker-3
        HostName 123.123.123.7
        User ubuntu
        IdentityFile ~/.ssh/keys/WorkerNode.pem
```

With this set up, you can ssh into the VM's with for example `ssh k3s-master-1`.


### Update hostname of machines
To keep things organized, ssh into each machine and change the hostname
```bash
sudo hostname <hostname>    # Change hostname to f.x k3s-worker-1 or k3s-master-1
sudo nano /etc/hostname     # Change the entry in the file to your <hostname>
```
You can exit and ssh to the node again to see the change


### Attach volumes to machines that have volumes
Your machines that run workloads, as well as gitlab runner, should preferably have a volume mounted to them. There are 
higher storage requirements for these particular machines, because they heavily depend on pulling docker images, which 
can quickly eat up disk space if left unattended (un-pruned). By using volumes, if we ever run out of storage it will
be easier to replace the volume or increase the size of it.

Here is how you attach a volume that is available to your machine, to the machine.
```bash
sudo lsblk                              # Find the name of the volume you want to attach (for example xvdb)
sudo mkfs -t xfs /dev/<volume_name>     # Format the volume for linux
sudo mkdir /data                        # Create a mounting directory for the volume called 'data'
sudo mount /dev/<volume_name> /data     # Mount the volume to the data directory
sudo chown -R $USER /data               # Give user permissions to volume directory
```


### SSH access from office machine
We have set up a `WORKER-NODE` security group which allows SSH access from the office. This however
is not enough to enable an SSH connection since we do not have a public key association with the cluster management
machine.

1. **On office machine**
   - If it's a Windows machine, use the git bash terminal
   - Check for an `id_rsa.pub` file in `~/.ssh` folder
   - If it does not exist generate it with the command `ssh keygen` and press enter on all prompts
   - Type `cat ~/.ssh/id_rsa.pub` and copy the public key printed in the terminal
2. **On Manager Node machine**
   - Run the following command in terminal `echo <The copied public key> >> ~/.ssh/authorized_keys; history -d $(history 1)` 
3. **On office machine**
   - If it's a Windows machine, use the git bash terminal
   - Create an entry in the SSH config file 
     1. Open or create file `~/.ssh/config`
     2. Add the following to the file (same values as used in Domains & DNS setup)
     ```text
     Host <customer>-cluster
        HostName cluster.manager.<customer>.<public-domain>
        User ubuntu
     ```
   - Test the connection with `ssh <customer>-cluster`


## 3. Master nodes
To support high availability, we need 3 master nodes. This means that the cluster is highly available only when there 
are at least 3 master nodes in the cluster, because only then there is not a single point of failure. Etcd requires 
at least 2 master nodes to recover from failures, which means with 3 master nodes, we can lose one without disrupting 
normal operation.

### Installation
From within your cluster management machine

**Note**: If you want to run workloads on your master nodes do not use the `--node-taint` flag and add the 
`--data-dir` flag explained in the Worker node section

1. Install K3S server on the **first master node**.
    <br>
    SSH into your master node
    ```bash
    ssh k3s-master-1
    ```
    Install K3S
    ```bash
    curl -sfL https://get.k3s.io | \
      INSTALL_K3S_VERSION="v1.25.2+k3s1" \
      sh -s - server \
      --cluster-init \
      --node-taint CriticalAddonsOnly=true:NoExecute \
      --tls-san <YOUR_MASTER_NODE_DOMAIN>
    ```
2. Retrieve the token from your first master node (_you will need it to set up the remaining nodes_)
    ```bash
    sudo cat /var/lib/rancher/k3s/server/node-token
    ```
3. Install K3S server on your **remaining master nodes**
    <br>
    SSH into remaining node
    ```bash
    ssh k3s-master-x
    ```
    Install K3S
    ```bash
    curl -sfL https://get.k3s.io | \
      INSTALL_K3S_VERSION="v1.25.2+k3s1" \
      K3S_TOKEN=<TOKEN_FROM_FIRST_MASTER_NODE> \
      sh -s - server \
      --server https://<IP_ADDRESS_OF_FIRST_MASTER_NODE>:6443 \
      --node-taint CriticalAddonsOnly=true:NoExecute \
      --tls-san <YOUR_MASTER_NODE_DOMAIN>
    ```
4. Check that everything is in order
    <br>
    View the available master nodes with kubectl
    ```bash
    sudo kubectl get nodes
    ```
    This should output something like this, which displays all the master nodes you registered.
    ```text
    ubuntu@k3s-master-1:~$ sudo kubectl get nodes
    NAME           STATUS   ROLES                       AGE    VERSION
    k3s-master-1   Ready    control-plane,etcd,master   3m    v1.25.2+k3s1
    k3s-master-2   Ready    control-plane,etcd,master   2m    v1.25.2+k3s1
    k3s-master-3   Ready    control-plane,etcd,master   1m    v1.25.2+k3s1
    ```

The `--node-taint CriticalAddonsOnly=true:NoExecute` flag, tells k3s to install the master node in such a way
that no workloads are scheduled on it. This is a kubernetes best practice, but can be ignored if desired. Just
remember that if you want to run workloads on your master nodes, it needs more storage and processing power.

The following commands should be used **instead of the commands above** if you are running master+worker on the same node. 
Refer to the worker node section on creating the `/data/docker/rancher/k3s` directory before running the commands.

- On the first node
    ```bash
    curl -sfL https://get.k3s.io | \
      INSTALL_K3S_VERSION="v1.25.2+k3s1" \
      sh -s - server \
      --cluster-init \
      --tls-san <YOUR_MASTER_NODE_DOMAIN> \
      --data-dir="/data/docker/rancher/k3s"
    ```
- On the remaining nodes
    ```bash
    curl -sfL https://get.k3s.io | \
      INSTALL_K3S_VERSION="v1.25.2+k3s1" \
      K3S_TOKEN=<TOKEN_FROM_FIRST_MASTER_NODE> \
      sh -s - server \
      --server https://<IP_ADDRESS_OF_FIRST_MASTER_NODE>:6443 \
      --tls-san <YOUR_MASTER_NODE_DOMAIN>
      --data-dir="/data/docker/rancher/k3s"
    ```

## 4. Worker nodes
Worker nodes run our kubernetes workloads. You would want to have at least 2 worker nodes to support high availability.

### Installation
From within your Cluster Management machine

1. SSH into worker node
    ```bash
    ssh k3s-worker-x
    ```
2. Create a directory inside the volume you mounted to the machine earlier. We will use this directory to store all docker 
   images for the worker
    ```bash
    mkdir -p /data/docker/rancher/k3s
    ```
3. Install K3S agent
    ```bash
    curl -sfL https://get.k3s.io | \
      INSTALL_K3S_VERSION="v1.25.2+k3s1" \
      K3S_URL=https://<IP_ADDRESS_OF_FIRST_MASTER_NODE>:6443 \
      K3S_TOKEN=<TOKEN_FROM_FIRST_MASTER_NODE> \
      sh -s - agent \
      --data-dir="/data/docker/rancher/k3s"
    ```
4. Check that everything is in order
    <br>
    Go back into one of your master nodes.
    ```bash
    ssh k3s-master-1
    ```
    View the available nodes with kubectl
    ```bash
    sudo kubectl get nodes
    ```
    This should output something like this, which displays all the new worker nodes you registered.
    ```text
    NAME           STATUS   ROLES                       AGE     VERSION
    k3s-master-1   Ready    control-plane,etcd,master   5m      v1.25.2+k3s1
    k3s-master-2   Ready    control-plane,etcd,master   4m      v1.25.2+k3s1
    k3s-master-3   Ready    control-plane,etcd,master   3m      v1.25.2+k3s1
    k3s-worker-1   Ready    <none>                      2m      v1.25.2+k3s1
    k3s-worker-2   Ready    <none>                      1m      v1.25.2+k3s1
    ```
   
The `--data-dir="/data/docker/rancher/k3s"` flag tells K3S to store docker images in the directory we created in the
volume mounted to our machine.

If you want to reconfigure where the images are stored, you can run the installation command again with a different 
data-dir value, then run `sudo systemctl restart k3s-agent.service` to make the changes take effect. It is probably
wise to copy the contents of the current directory used by k3s into your new directory if you want existing
resources to work as expected.

## 5. Connect kubeconfig to cluster manager
We want to be able to run kubectl commands from our cluster manager without needing to ssh into master nodes.
For this, use the script [manager_setup.sh](infrastructure/scripts/manager_setup.sh). It should install all the required
packages inside our cluster manager. There might be some updates on the systemd so if those pop up just press ok (enter).

> `$ bash manager_setup.sh`

When the packages have installed, the script prompts you to enter the following information:
1. The SSH alias you configured for one of your master nodes (For example `k3s-master-1`)
2. The domain you configured to route to your master nodes (`<customer>.<internal-domain>`)
3. The name of the customer you are setting up the cluster for. (`<customer>`)

When the script finishes, you should be able to run `kubectl` commands directly from your cluster manager machine.
You can test it by running `kubectl cluster info`.

## 6. Register kubernetes cluster on gitlab
To use kubectl from our gitlab pipeline, we need to register our agent on gitlab. This allows us to apply a kubeconfig
during job execution with the command `kubectl config use-context $CI_PROJECT_PATH:<cluster-agent-name>` which 
gives us the required authorization and a connection to the cluster.

We can register our agent on gitlab from within our cluster manager with a [helm](https://helm.sh/) deployment.
1. **Within this gitlab project's UI:**
  - Navigate to `infrastructure -> kubernetes cluster`
  - Press connect a cluster
  - Enter a new agent in the input with name `<customer>-agent` and click create
  - The helm command to run in the cluster manager's terminal is displayed
  - Copy the helm command 
2. **Within cluster manager machine:**
  - Paste the helm command in terminal and run it
3. **Within this gitlab project:**
   - See the active status to green

## 7. Gitlab runner
In order for our cluster to remain on a private network, while using CI/CD on gitlab to deploy to it, we need to have 
our own CI/CD runner on the same network as the cluster. Gitlab makes this quite simple with self-hosted runners. 
Gitlab runner is a poll based system, which means that the runner does not need to allow incoming traffic from the
internet to work. It polls the git repositories on an interval to check for any CI/CD jobs that need to be executed.

### Installation
1. SSH into gitlab runner machine
    ```bash
    ssh gitlab-runner
    ```
2. Use the installation script [install_gitlab_runner.sh](infrastructure/scripts/register_gitlab_runner.sh) and wait for 
   the installation to complete. 
    ```bash
    sudo bash install_gitlab_runner.sh
    ```
3. Reboot your server to make sure docker will work properly
    ```bash
    sudo reboot now
    ```
4. Register your runner with the registration script [register_gitlab_runner.sh](infrastructure/scripts/register_gitlab_runner.sh)
    ```bash
    bash register_gitlab_runner.sh
    ```
   The script prompts you to enter the following information:
   1. Runner registration token: Copy+Paste a [registration token](https://docs.gitlab.com/runner/register/) from gitlab
   2. The customer name: F.x `demo` or `dev`

**NOTE**: 
- If the gitlab runner doesn't have an 'online' status in the Gitlab UI within a minute, 
you can try running `sudo gitlab-runner restart`
- You can register multiple runners on the same machine


## SSL Certification
Setting up and managing SSL has never been easier than in a kubernetes cluster using cert-manager with letsencrypt.
We will be utilizing AWS Route53 to allow letsencrypt to verify that we own the domain.

### Auth for certificate generation (IAM in AWS)
For this setup we need to have an IAM user with an IAM policy that allows it to read and write specific resources in 
Route53.

#### Create an IAM policy (IAM Policies in AWS)
The following IAM policy will give sufficient permissions for the IAM user we will create later. You can insert
this in JSON format into the creation tab.

Replace `<PUBLIC_HOSTED_ZONE_ID>` with the id of the public hosted zone you created in Route53 for your cluster.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "route53:GetChange",
            "Resource": "arn:aws:route53:::change/*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "route53:ChangeResourceRecordSets",
                "route53:ListResourceRecordSets"
            ],
            "Resource": "arn:aws:route53:::hostedzone/<PUBLIC_HOSTED_ZONE_ID>"
        },
        {
            "Effect": "Allow",
            "Action": "route53:ListHostedZonesByName",
            "Resource": "*"
        }
    ]
}
```

#### Create the IAM user
Later in our cert-manager setup, we will need aws authentication credentials on the format 
`AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`. This is something we can obtain by creating a IAM user on amazon.

We create an IAM user with the credential type `Access Key - Programmatic access` and then attach the policy we created
in the previous step to the user.

Once done, we can download the credentials, which should contain the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.

### CertManager setup
cert-manager adds certificates and certificate issuers as resource types in Kubernetes clusters, and simplifies the 
process of obtaining, renewing and using those certificates.

The following steps (commands to run) should all be done within the **Manager machine** of your cluster.

#### Install cert-manager
Install the latest version of cert-manager with this command
```bash
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.10.0/cert-manager.yaml
```

#### Create cluster secret for IAM user
We now have a secret access key that allows cert-manager to authenticate with aws. **We do not want to store this in plain text**. We begin by creating a secret for the `AWS_SECRET_ACCESS_KEY`.

1. Copy the contents of [01-aws-secret.yml](infrastructure/tls/01-aws-secret.yml)
2. Replace `<SECRET_ACCESS_KEY_ID>` with your `AWS_SECRET_ACCESS_KEY`.
3. Create the secret on the cluster
    ```bash
    kubectl apply -f 01-aws-secret.yml
    ```
4. When done, remove the file
    ```bash
    rm 01-aws-secret.yml
    ```

#### Setup letsencrypt staging and test it
Before we start issuing real certificates, we are going to use lets-encrypts staging server to test whether our setup
is valid. This is mainly because there are rate limits on certificates which we do not want to exceed by accident.

1. Deploy staging cluster issuer
   1. Copy the contents of [02-staging-cluster-issuer.yml](infrastructure/tls/02-staging-cluster-issuer.yml) 
   2. Replace `<IAM_USER_ACCESS_KEY_ID>` with your `AWS_ACCESS_KEY_ID`
   3. Replace `<TENANT_AWS_REGION>` with the aws region of the AWS tenant
   4. Replace `<ROUTE_53_HOSTED_ZONE_ID>` with the id of the **public hosted zone** you created for the cluster
   5. Create the staging cluster issuer
       ```bash
       kubectl apply -f 02-staging-cluster-issuer.yml
       ```
   6. Wait for the cert manager to be in READY state
       ```bash
       kubectl get clusterissuer
       ```
2. Deploy staging domain certificate
   1. Copy the contents of [03-staging-domain-certificate.yml](infrastructure/tls/03-staging-domain-certificate.yml)
   2. Replace `<customer>.<public_domain>` with your domain record
   3. Replace `*.<customer>.<public_domain>` with our wildcard record
   4. Create the staging domain certificate
       ```bash
       kubectl apply -f 03-staging-domain-certificate.yml
       ```
   5. Wait for the domain certificate to become available (This can take a few minutes!)
       ```bash
       kubectl get certificate
       ```
3. Deploy sample application to test https
   1. Copy the contents of [04-staging-example-app.yml](infrastructure/tls/04-staging-example-app.yml)
   2. Replace `<customer>.<public_domain>` with your domain record **(Two places in file)**
   3. Replace `<customer>` **(One place in file)**
   4. Create the app
       ```bash
       kubectl apply -f 04-staging-example-app.yml
       ```
   5. Wait for the app to be ready
       ```bash
       kubectl get pods -l app=httpbin,certType=staging
       ```
   6. Go to `https://httpbin.<customer>.<public_domain>` and see the TLS active. 
      The browser will most likely say `insecure` because we are using a staging certificate.
   7. Remove the staging httpbin app when done
        ```bash
       kubectl delete all -l app=httpbin,certType=staging
       ```
   8. (Optional) Remove all the staging cert resources when done
       ```bash
       kubectl delete all -l certType=staging --all-namespaces
       ```   
      
#### Set up production certificates
Now that we have made sure our certificates are working we set up the production certificates.

1. Deploy production cluster issuer
   1. Copy the contents of [05-prod-cluster-issuer.yml](infrastructure/tls/05-prod-cluster-issuer.yml) 
   2. Replace `<IAM_USER_ACCESS_KEY_ID>` with your `AWS_ACCESS_KEY_ID`
   3. Replace `<TENANT_AWS_REGION>` with the aws region of the AWS tenant
   4. Replace `<ROUTE_53_HOSTED_ZONE_ID>` with the id of the **public hosted zone** you created for the cluster
   5. Create the staging cluster issuer
       ```bash
       kubectl apply -f 05-prod-cluster-issuer.yml
       ```
   6. Wait for the cert manager to be in READY state
       ```bash
       kubectl get clusterissuer
       ```
2. Deploy production domain certificate
   1. Copy the contents of [06-prod-domain-certificate.yml](infrastructure/tls/06-prod-domain-certificate.yml)
   2. Replace `<customer>.<public_domain>` with your domain record
   3. Replace `*.<customer>.<public_domain>` with our wildcard record
   4. Create the production domain certificate
       ```bash
       kubectl apply -f 06-prod-domain-certificate.yml
       ```
   5. Wait for the domain certificate to become available (This can take a few minutes!)
       ```bash
       kubectl get certificate
       ```
3. Deploy sample application to check production https
   1. Copy the contents of [07-prod-example-app.yml](infrastructure/tls/07-prod-example-app.yml)
   2. Replace `<customer>.<public_domain>` with your domain record **(Two places in file)**
   3. Replace `<customer>` **(One place in file)**
   4. Create the app
       ```bash
       kubectl apply -f 07-prod-example-app.yml
       ```
   5. Wait for the app to be ready
       ```bash
       kubectl get pods -l app=httpbin,certType=production
       ```
   6. Go to `https://httpbin.<customer>.<public_domain>` and see the TLS active. The browser should no longer show as insecure.
   7. Remove the httpbin app when done
       ```bash
       kubectl delete all -l app=httpbin,certType=production
       ```
   

### Extra: Labeling your nodes
If you want to be able to select a specific node for some deployment, for example if your node needs to meet some 
hardware requirements, you can run this command from your cluster manager to add a label to your node.

Command:
> `$ kubectl label node <NODE_HOSTNAME> <LABEL_NAME>=<LABEL VALUE>`

Example:
> `$ kubectl label node k3s-worker-1 allowTraining=true`

In pod specifications for a deployment, at the same level as the `container` key is defined, you can
then add a `nodeSelector` so that your pods will only be allowed to run on those specific nodes.

For example:
```yaml
nodeSelector:
  allowTraining: "true"
```
