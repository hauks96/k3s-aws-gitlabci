apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: staging-cert-manager-acme-issuer
  # Important: use the same namespace as Cert Manager deployment
  # Otherwise Cert Manager won't be able to find related elements
  namespace: cert-manager
  labels:
    certType: "staging"
spec:
  acme:
    # Email on which you'll receive notification for our certificates (expiration and such)
    email: domains@dte.ai
    # Name of the secret under which to store the secret key used by acme
    # This secret is managed by ClusterIssuer resource, you don't have to create it yourself
    privateKeySecretRef:
      name: staging-cert-manager-acme-private-key
    # ACME server to use
    # Specify https://acme-v02.api.letsencrypt.org/directory for production
    # Staging server issues staging certificate which won't be trusted by most external parties but can be used for development purposes
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    # Solvers define how to validate you're the owner of the domain for which to issue certificate
    # We use DNS-01 challenge with Route53 by providing related AWS credentials (access key and secret key) for an IAM user with proper rights to manage Route53 records
    # DNS-01 challenge is used for wildcard certificates
    solvers:
    - dns01:
        route53:
          # AWS Access Key ID for our Secret Key
          accessKeyID: <IAM_USER_ACCESS_KEY_ID>
          # AWS region to use
          region: <TENANT_AWS_REGION>
          # Reference our secret with Secret Key
          secretAccessKeySecretRef:
            key: secret-access-key
            name: cert-manager-aws-secret
          # Optionally specify Hosted Zone
          # As per doc:
          # If set, the provider will manage only this zone in Route53 and will not do a lookup using the route53:ListHostedZonesByName api call.
          hostedZoneID: <ROUTE_53_HOSTED_ZONE_ID>