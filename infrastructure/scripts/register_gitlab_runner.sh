#!/bin/bash

read -r -p "Enter registration token    : " GITLAB_REGISTRATION_TOKEN
read -r -p "Enter deployment target name: " RUNNER_TARGET

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "$GITLAB_REGISTRATION_TOKEN" \
  --executor "docker" \
  --docker-image debian:latest \
  --description "Runner for kubernetes deployments to the $RUNNER_TARGET environment" \
  --maintenance-note "Runs with debian latest image" \
  --tag-list "$RUNNER_TARGET-k8s-runner" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="ref_protected" \
  --shell="bash"

