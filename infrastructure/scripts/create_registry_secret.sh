#!/bin/bash

# Script that generates a gitlab container registry pull secret to use with gitlab-pull-secret.yml

if [ "$#" -ne 1 ]; then
    printf "Invalid number of arguments" >&2
    printf "./create_registry_secret.sh <GITLAB_DEPLOY_TOKEN>" >&2
    exit 1;
fi

secret_gen_string='{"auths":{"https://registry.gitlab.com":{"username":"{{USER}}","password":"{{TOKEN}}","email":"{{EMAIL}}","auth":"{{SECRET}}"}}}'

gitlab_token=$1
gitlab_user="deploy-token-name"
gitlab_email="your-email@example.com"
gitlab_secret=$(echo -n "$gitlab_user:$gitlab_token" | base64 -w 0)

echo -n "$secret_gen_string" \
    | sed "s/{{USER}}/$gitlab_user/" \
    | sed "s/{{TOKEN}}/$gitlab_token/" \
    | sed "s/{{EMAIL}}/$gitlab_email/" \
    | sed "s/{{SECRET}}/$gitlab_secret/" \
    | base64 -w 0

