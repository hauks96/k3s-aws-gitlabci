#!/bin/bash

# Install kubectl on the  cluster manager machine
if [ ! -f "/usr/local/bin/kubectl" ]; then
  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
  sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
else
  echo "Skipping kubectl install: Already installed"
fi

# Install helm (to register agent on gitlab)
helm > /dev/null
exit_code="$(echo $?)"

if [ "$exit_code" -ne "0" ]; then
  curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
  sudo apt-get install apt-transport-https --yes
  echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
  sudo apt-get update
  sudo apt-get install helm
else
  echo "Skipping helm install: Already installed"
fi;

mkdir -p ~/.kube
touch ~/.kube/config

read -r -p "Enter ssh alias name for a master server node           : " SSH_TARGET
read -r -p "Enter configured internal domain for master nodes       : " CLUSTER_MASTER_DOMAIN
read -r -p "Enter the name of the deployment target for this cluster: " DEPLOYMENT_TARGET

local_config="$(ssh -t "$SSH_TARGET" 'sudo cat /etc/rancher/k3s/k3s.yaml')"
external_config=${local_config//127.0.0.1/$CLUSTER_MASTER_DOMAIN}
config=${external_config//"default"/"$DEPLOYMENT_TARGET-cluster"}

echo "$config" > ~/.kube/config
chmod 600 ~/.kube/config

echo "CLUSTER STATUS"
kubectl cluster-info 2> /dev/null || (echo "Unable to reach cluster with 'kubectl cluster-info'" && exit 0;)
